#### ****************************************** Import ************************************* ### 

#import coloredlogs, logging
import sys
import time
import pickle
import datetime
import csv
import shutil
import os
import math
from collections import deque

#### ****************************************** File inclusion ************************************* ### 


### ***************************************** Target selection ************************************* ###

TARGET_DEVICE_PC = 1
TARGET_DEVICE_CALIXTO_GATEWAY = 2

TARGET_DEVICE = TARGET_DEVICE_PC
#TARGET_DEVICE = TARGET_DEVICE_CALIXTO_GATEWAY

### ****************************************** Constant declaration ************************************* ###

READER_RESTRICTION_AGE = "02"
READER_RESTRICTION_GENDER = "01"

READER_MESSAGE_TYPE_TRACKING            = "16"
READER_MESSAGE_TYPE_NFC                 = "18"
READER_MESSAGE_TYPE_HEARTBEAT           = "15"
READER_MESSAGE_TYPE_HEARTBEAT_RESPONSE  = "12"
READER_MESSAGE_TYPE_PANIC               = "17"
READER_MESSAGE_TYPE_HARDWARE_ALERT      = "19"
READER_MESSAGE_TYPE_CONFIGURE           = "01"
READER_MESSAGE_TYPE_CONFIGURE_RESPONSE  = "08"
READER_MESSAGE_TYPE_STOP_BEACONING      = "04"
READER_MESSAGE_TYPE_STOP_SCANNING       = "05"
READER_MESSAGE_TYPE_START_BEACONING     = "02"
READER_MESSAGE_TYPE_START_SCANNING      = "03"


CLOUD_MESSAGE_CLASS_FUNCTIONING    = "1"
CLOUD_MESSAGE_CLASS_CONFIGURATION  = "2"

CLOUD_MESSAGE_TYPE_TRACKING_FILE_UPLOAD = "01"
CLOUD_MESSAGE_TYPE_PANIC                = "02"
CLOUD_MESSAGE_TYPE_READER_ALERT         = "03"
CLOUD_MESSAGE_TYPE_READER_STATUS        = "04"
CLOUD_MESSAGE_TYPE_GATEWAY_ALERT        = "05"
CLOUD_MESSAGE_TYPE_GATEWAY_ALIVE        = "06"
CLOUD_MESSAGE_TYPE_RESTRICTION_ALERT    = "08"
CLOUD_MESSAGE_TYPE_ACK                  = "09"
CLOUD_MESSAGE_TYPE_NACK                 = "10"
CLOUD_MESSAGE_TYPE_CLOUD_ALERT          = "07"

CLOUD_MESSAGE_TYPE_CONFIGURE            = "01"
CLOUD_MESSAGE_TYPE_DOWNLOAD_STUDENT_IDS = "02"
CLOUD_MESSAGE_TYPE_UPDATE_STUDENT_IDS   = "03"
CLOUD_MESSAGE_TYPE_DOWNLOAD_ALERT_RULES = "04"
CLOUD_MESSAGE_TYPE_FIRMWARE_UPDATE      = "05"
CLOUD_MESSAGE_TYPE_STATUS_UPDATE        = "06"

#### ******************************** Circular buffer *****************************************####
Reader_receive_circular_buffer   = deque(list(), maxlen=20)
Cloud_send_circular_buffer       = deque(list(), maxlen=20)

### ****************************************** Variable declaration ************************************* ###
Gateway_management_data=list()
Reader_management_data=list()
Student_ids=list()
Student_ID_table=list()
Status_LED_pattern_interval=[2]
LED_status=["1"]
Gateway_hardware_error=[0]
student_entry_reported=[0]
sftp_server_details=["13.233.23.189",22,"bhumithisftp","SxzsCsIym1rosskFgBeTpXhM"]
Reader_sequence_number=[0]                       
Gateway_error_indication=[0,0,0]   #[Reader,cloud,hardware]

GATEWAY_ID = ["ID:"]
csv_file_generation_interval=[10*60]
Gateway_alive_interval=[30*60]
Reader_count=[2]
Reader_connection_retry_count=[2]
Reader_connection_retry_interval=[3]
Student_count=[1]
Student_Tracking_table_template=list()
Student_Tracking_table_dummy=['61606875656667','-','-','-','-','-','-','-']
Student_beacon_index=[86,112,138,164,190,216,242,268,294,320,346,372,398,424,450,476,502,528,554,580,606,632,658,684,710]

                     
### ******************************************** Function prototype ************************************* ###  
 

#************************ application initialization ********************************#

def LEDs_init():   
  #----------------------LED2------------------------------------------------#
  led_2_export=open("/sys/class/gpio/export", "w")
  led_2_export.write("%d" % (117))
  led_2_export.close()

  led_2_direction=open("/sys/class/gpio/gpio117/direction", "w")
  led_2_direction.write("out")
  led_2_direction.close()

#************************ application initialization ********************************#
def LEDs_deinit():
  try:
   led_2_unexport = open("/sys/class/gpio/unexport", "w")
   led_2_unexport.write("%d" % (117))
   led_2_unexport.close()
  except:
   print("Error")
 
#************************ application initialization ********************************#
def LEDs_pattern_indicator_thread(thread_name):
 
 while True:
  led_2=open("/sys/class/gpio/gpio117/value", "w")
  if LED_status[0]=='1':
   led_2.write("1")
   LED_status[0]='0'
  else:
   led_2.write("0")
   LED_status[0]='1'
  led_2.close()
  
  if Gateway_error_indication[0] == 2:               #Reader Communication error
    Status_LED_pattern_interval[0]=0.05
  elif Gateway_error_indication[1] == 1:             #Cloud Communication error
    Status_LED_pattern_interval[0]=0.003
  elif Gateway_error_indication[2] == 1:             #Gateway Hardware Error
    Status_LED_pattern_interval[0]=1
  else:
    Status_LED_pattern_interval[0]=2



#*******************************************************************************
  # @brief  Clear student tracking table.
  # @param  None
  # @retval None
  #****************************************************************************#

def Clear_student_track_table():
  for i in range(Student_count[0]):
    for j in range(7):
     Student_Tracking_table_template[i][j+1]='-'
  #print("Cleared Table",Student_Tracking_table)

#******************** End of Clear student tracking table ********************#

#*******************************************************************************
  # @brief  file write.
  # @param  filename, data structure to write
  # @retval None
  #****************************************************************************#
def CSV_file_write (file_name,data_structure,lines):
  with open(file_name, 'w', newline='') as writeFile:
    writer = csv.writer(writeFile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for i in range(lines):
        writer.writerow(data_structure[0][i])
  writeFile.close()

  print("CSV> File write success ")

#******************** End of gateway configuration store ********************#



#*******************************************************************************
  # @brief  Application initialization.
  # @param  None
  # @retval None
  #****************************************************************************#
  
def application_init():
  if TARGET_DEVICE == TARGET_DEVICE_CALIXTO_GATEWAY:
    LEDs_deinit()
    LEDs_init()
 #--------------------- Read gateway management data -------------------------------#
  if os.path.exists("./Data_storage/Device_management/Gateway_management.csv"):  ### Check Gateway management configuration file is there or not
    csv.register_dialect('myDialect',delimiter = ',',skipinitialspace=True)
    with open('./Data_storage/Device_management/Gateway_management.csv', 'r') as csvFile:
      reader = csv.reader(csvFile, dialect='myDialect')   
      Gateway_management_data.append(list(reader))
    csvFile.close()

        #---------------- Populating data on variable --------------------#
    GATEWAY_ID[0]                       ="ID:"+ Gateway_management_data[0][1][0]
    Student_count[0]                    =int(Gateway_management_data[0][1][12])
    csv_file_generation_interval[0]     =int(Gateway_management_data[0][1][13])*60
    Gateway_alive_interval[0]           =int(Gateway_management_data[0][1][14])*60
    Reader_count[0]                     =int(Gateway_management_data[0][1][4])
    Reader_connection_retry_count[0]    =3
    Reader_connection_retry_interval[0] =3
    #print("Gateway_management:",Gateway_management_data)
  else:
    print("Gateway_management File not exist")
  

  #--------------------- Read Reader management data -------------------------------#

  if os.path.exists("./Data_storage/Device_management/Reader_management.csv"):
    csv.register_dialect('myDialect',delimiter = ',',skipinitialspace=True)
    with open('./Data_storage/Device_management/Reader_management.csv', 'r') as csvFile:
      reader = csv.reader(csvFile, dialect='myDialect')   
      Reader_management_data.append(list(reader))

    csvFile.close()
    #print("Reader_management:",Reader_management_data)
  else:
    print("Reader_management file not found")


  #---------------------Read Student Ids from Student_ids.csv file --------------------------#


  if os.path.exists("./Data_storage/Device_management/Student_ids.csv"):
    csv.register_dialect('myDialect',delimiter = ',',skipinitialspace=True)
    with open('./Data_storage/Device_management/Student_ids.csv', 'r') as csvFile:
      reader = csv.reader(csvFile, dialect='myDialect') 
      Student_ids.append(list(reader))  
    csvFile.close()
    #print("student_management:",Student_ids)

  #--------------------- Insert Student ids to Tracking table -------------------------------#
    for i in range(Student_count[0]):
      Student_Tracking_table_dummy[0]=Student_ids[0][i][0]
      Student_Tracking_table_template.append(list(Student_Tracking_table_dummy))
    #print("Table:",Student_Tracking_table_template)
  else:
    print("Student IDs file not found")

#******************** End of Application initialization ********************#


#*******************************************************************************
  # @brief  Store gateway configuration details.
  # @param  configuration data
  # @retval None
  #****************************************************************************#
def Store_Gateway_configuration(gateway_config_data_json):
  Gateway_management_data[0][1][3]=gateway_config_data_json["GLN"]
  Gateway_management_data[0][1][4]=gateway_config_data_json["RCT"]
  Gateway_management_data[0][1][13]=gateway_config_data_json["TRI"]
  Gateway_management_data[0][1][14]=gateway_config_data_json["GAI"]
  Reader_configuration_data=gateway_config_data_json["RDR"]
        
  for i in range (int(Gateway_management_data[0][1][4])):
    #Reader_configuration_data_json= json.loads(Reader_configuration_data[i],encoding='utf-8')
    Reader_management_data[0][i+1][0] = Reader_configuration_data[i]["RID"]
    Reader_management_data[0][i+1][4] = Reader_configuration_data[i]["STV"]
    Reader_management_data[0][i+1][5] = Reader_configuration_data[i]["TTV"]
    Reader_management_data[0][i+1][6] = Reader_configuration_data[i]["HBT"]
    Reader_management_data[0][i+1][7] = Reader_configuration_data[i]["BTI"]
    Reader_management_data[0][i+1][8] = Reader_configuration_data[i]["BCT"]

  if os.path.exists("./Data_storage/Device_management/Reader_management.csv"):
    os.remove("./Data_storage/Device_management/Reader_management.csv")
  else:
    print("The file does not exist")

    #print("Reader data : ",Reader_management_data)
  CSV_file_write('./Data_storage/Device_management/Reader_management.csv',Reader_management_data,int(Gateway_management_data[0][1][4])+1)
  

  if os.path.exists("./Data_storage/Device_management/Gateway_management.csv"):
    os.remove("./Data_storage/Device_management/Gateway_management.csv")
  else:
    print("The file does not exist")

  CSV_file_write('./Data_storage/Device_management/Gateway_management.csv',Gateway_management_data,2)
        
  print("Cloud Module> CONFIGURE Packet received")
  #print("Gateway> gateway configuration: ",gateway_config_data_json)

#******************** End of store gateway configuration ********************#


#*******************************************************************************
  # @brief  Store reader configuration details.
  # @param  configuration data
  # @retval None
  #****************************************************************************#
def Store_reader_configuration(reader_config_data_json):
  print("Gateway> reader configuration: ",reader_config_data_json)

#******************** End of store reader configuration ********************#


#*******************************************************************************
  # @brief  Store student ids and details
  # @param  Student details
  # @retval None
  #****************************************************************************#
def Store_Student_ids(student_IDs_packet_json):
  student_IDs_json=student_IDs_packet_json["SDT"]
  Gateway_management_data[0][1][12]=student_IDs_packet_json["SCT"]

  for i in range (int(Gateway_management_data[0][1][12])):
    Student_ids[0][i][0]=student_IDs_json[i]["SID"]
    Student_ids[0][i][1]=student_IDs_json[i]["AGE"]
    Student_ids[0][i][2]=student_IDs_json[i]["GEN"]


  if os.path.exists("./Data_storage/Device_management/Gateway_management.csv"):
    os.remove("./Data_storage/Device_management/Gateway_management.csv")
  else:
    print("The file does not exist")

  CSV_file_write('./Data_storage/Device_management/Gateway_management.csv',Gateway_management_data,2)
  #print("Student > Gateway_management: ",Gateway_management_data)

  if os.path.exists("./Data_storage/Device_management/Student_ids.csv"):
    os.remove("./Data_storage/Device_management/Student_ids.csv")
  else:
    print("The file does not exist")

  CSV_file_write('./Data_storage/Device_management/Student_ids.csv',Student_ids,int(Gateway_management_data[0][1][12]))
  #print("Student > student ids: ",Student_ids)

        

#******************** End of store dtudent ids ********************#

#*******************************************************************************
  # @brief  Store alert rules for reader.
  # @param  alert rules details
  # @retval None
  #****************************************************************************#
def Store_alert_rules(alert_rules_packet_json):
  alert_rules_json=alert_rules_packet_json["ALT"]
  for j in range (int(alert_rules_packet_json["CNT"])):                      ### Alert rule count loop
    for i in range (int(Gateway_management_data[0][1][4])):                  ### Reader count loop
      if alert_rules_json[j]["RID"] == Reader_management_data[0][i+1][0]:    ### Check the reader Id in our list
          Reader_management_data[0][i+1][11]=alert_rules_json[j]["RUL"]
          if alert_rules_json[j]["RUL"] == "01":                             ### Check Geneder restriction 
            Reader_management_data[0][i+1][14]=alert_rules_json[j]["GEN"]
            Reader_management_data[0][i+1][12]=''
            Reader_management_data[0][i+1][13]=''
            break
          elif alert_rules_json[j]["RUL"]== "02":                            ### Check Age restriction
            Reader_management_data[0][i+1][12]=alert_rules_json[j]["AGH"]
            Reader_management_data[0][i+1][13]=alert_rules_json[j]["AGL"]
            Reader_management_data[0][i+1][14]=''
            break
  if os.path.exists("./Data_storage/Device_management/Reader_management.csv"):
    os.remove("./Data_storage/Device_management/Reader_management.csv")
  else:
    print("The file does not exist")

  CSV_file_write('./Data_storage/Device_management/Reader_management.csv',Reader_management_data,Reader_count[0]+1)
  #print("Store> Reader management data: ",Reader_management_data)

#******************** End of store alert rules ********************#
