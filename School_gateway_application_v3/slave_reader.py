# Socket client example in python

import socket
import sys  
import _thread
import time
import json
import pickle
import datetime
import csv
import shutil
import os

#### ****************************************** File inclusion ************************************* ### 
from cloud_communication import *
from common_include import *


### ****************************************** Variable declaration ************************************* ###
Reader_sockets=[0,0,0,0]
Reader_data_valid_flag=[0]


SG_id = ["010000005"]
SG_type = ["03"]
Local_time = ["015245"]
Local_date = ["29102018"]
seqn = ["0014"]
reader_rx_packet_raw =["{}"]
reader_rx_packet_json={"test":"test"}
reader_tx_packet_raw = ["{\"SGID\":\""+SG_id[0]+"\",\"TYPE\":\""+SG_type[0]+"\",\"TIME\":\""+Local_time[0]+"\",\"DATE\":\""+Local_date[0]+"\",\"SEQN\":\""+seqn[0]+"\"}"]
reader_tx_packet_encoded = [reader_tx_packet_raw[0].encode('utf-8')]
reader_valid_packet_types=['15']

Student_beacon_packet_string=["61606875656667","01","012045","0801","100","103"]
Student_beacon_packet_HEX=["61606875656667","01","012045","0801","100","103"]


### ******************************************** Function prototype ************************************* ###


#*******************************************************************************
  # @brief  Check the position of student.
  # @param  beacon rssi
  # @retval None
  #****************************************************************************#

def student_position_check(student_beacon_rssi):
  measured_power = -65
  N=2
  result1 = (measured_power-student_beacon_rssi)/(10*N)
  result = math.pow(10,result1)
  if result <=2:
    return 1
  else:
    return 0

#******************** End of student position check ********************#


#*******************************************************************************
  # @brief  Reader Transmit packet Formation.
  # @param  beacon rssi
  # @retval None
  #****************************************************************************#

def Reader_transmit_packet_form():
    now = datetime.datetime.now()
    Local_date[0]=str('{0:02d}'.format(now.day))+str('{0:02d}'.format(now.month))+str('{0:04d}'.format(now.year))
    Local_time[0]=str('{0:02d}'.format(now.hour))+str('{0:02d}'.format(now.minute))+str('{0:02d}'.format(now.second))
    reader_tx_packet_raw[0] = "{\"SGID\":\""+SG_id[0]+"\",\"TYPE\":\""+SG_type[0]+"\",\"TIME\":\""+Local_time[0]+"\",\"DATE\":\""+Local_date[0]+"\",\"SEQN\":\""+seqn[0]+"\"}"

#******************** End of Reader transmit packet form ********************#


#*******************************************************************************
  # @brief  Reader send data.
  # @param  short id, send data
  # @retval None
  #****************************************************************************#

def Reader_send_data(Reader_short_id,send_data):
  try:
    Reader_sockets[Reader_short_id].sendall(send_data.encode('utf-8'))
  except socket.error as e:
    print ("send error: %s", e)

#******************** End of Reader send data ********************#


            
#*******************************************************************************
  # @brief  String to signed integer convertor.
  # @param  value
  # @retval None
  #****************************************************************************#
def s16(value):
    return -(value & 0x8000) | (value & 0x7fff)

#************************************* End of s16 *****************************#


#*******************************************************************************
  # @brief  Check Rstriction alert process.
  # @param  reader id, student id, beacon time
  # @retval None
  #****************************************************************************#
def Reader_check_restriction_alert(reader_id,student_id,beacon_time):
    for i in range(Student_count[0]):
      if Student_ids[0][i][0] == student_id:
        student_gender=Student_ids[0][i][2]
        student_age=Student_ids[0][i][1]
        break

    for i in range (Reader_count[0]):
      if Reader_management_data[0][i+1][0] == reader_id:
        if Reader_management_data[0][i+1][11] == READER_RESTRICTION_AGE:
          #print("On check")
          if int(student_age) <= int(Reader_management_data[0][i+1][12]) and int(student_age) >= int(Reader_management_data[0][i+1][13]) :    
            m_data_payload="\"SID\":\""+student_id+"\",\"RID\":\""+reader_id+"\",\"RTP\":\""+READER_RESTRICTION_AGE+"\""
            #print("Reader> Age restricted reader -- age between ",Reader_management_data[0][i+1][12],Reader_management_data[0][i+1][13])
            cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_RESTRICTION_ALERT,m_data_payload]
            Cloud_send_circular_buffer.append(cloud_tx_data)

        if Reader_management_data[0][i+1][11] == READER_RESTRICTION_GENDER:
          if student_gender == Reader_management_data[0][i+1][14]:
            m_data_payload="\"SID\":\""+student_id+"\",\"RID\":\""+reader_id+"\",\"RTP\":\""+READER_RESTRICTION_GENDER+"\""
            #print("Reader> gender restricted reader -- ", Reader_management_data[0][i+1][14])

        #cloud_data_send(CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_RESTRICTION_ALERT,m_data_payload)
            cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_RESTRICTION_ALERT,m_data_payload]
            Cloud_send_circular_buffer.append(cloud_tx_data)
        break


#******************** End of Reader check restriction alert ********************#


#*******************************************************************************
  # @brief  Tracking packet process.
  # @param  short id, reader received data
  # @retval None
  #****************************************************************************#

def Reader_tracking_packet_decode(Reader_short_id,Reader_received_data):
  #print("Tracking packet:",Reader_received_data)
  student_packet = bytearray(16)
  temp=bytearray(2)
  time = ["01","02","03"]
  temp[0]=Reader_received_data[74]
  temp[1]=Reader_received_data[75]
  
  for i in range(int(temp.decode('utf-8'))):
    for j in range(16):
      student_packet[j]=Reader_received_data[Student_beacon_index[i]+j]
    Beacon_packet_string=student_packet.hex()
    Student_beacon_packet_HEX[0]=Beacon_packet_string[0:14]     # Student ID
    Student_beacon_packet_HEX[1]=Beacon_packet_string[14:16]    # Beacon status
    Student_beacon_packet_HEX[2]=Beacon_packet_string[16:22]    # Time
    Student_beacon_packet_HEX[3]=Beacon_packet_string[22:28]    # Date
    Student_beacon_packet_HEX[4]=Beacon_packet_string[28:30]    # Battery voltage in percentage
    Student_beacon_packet_HEX[5]=Beacon_packet_string[30:32]    # RSSI
    
    rssi_byte_array_string=hex(student_packet[15]|0xFF00)
    temp=s16(int(rssi_byte_array_string, 16))

    if student_position_check(temp) == 1:
      print("Inside Reader circle limit")
      for j in range (Student_count[0]):
        if Student_Tracking_table_template[j][0]==Student_beacon_packet_HEX[0]:
          student_entry_reported[0]=1
          if Student_Tracking_table_template[j][1] == '-':
            Student_Tracking_table_template[j][3]=Student_Tracking_table_template[j][1]=Reader_received_data[9:18].decode('utf-8')
            Student_Tracking_table_template[j][4]=Student_Tracking_table_template[j][2]=str('{0:02d}'.format(student_packet[8]))+str('{0:02d}'.format(student_packet[9]))+str('{0:02d}'.format(student_packet[10]))
          else:
            Student_Tracking_table_template[j][3]=Reader_received_data[9:18].decode('utf-8')
            Student_Tracking_table_template[j][4]=str('{0:02d}'.format(student_packet[8]))+str('{0:02d}'.format(student_packet[9]))+str('{0:02d}'.format(student_packet[10]))
        
          Student_Tracking_table_template[j][5]='-'
          Student_Tracking_table_template[j][6]='-'
          Student_Tracking_table_template[j][7]=student_packet[14]
          Reader_check_restriction_alert(Student_Tracking_table_template[j][3],Student_Tracking_table_template[j][0],Student_Tracking_table_template[j][4])
    else:
      print("Outside Reader circle limit")
      continue

    #print("Tracking table ",Student_Tracking_table_template)

#******************** End of Reader tracking packet decode ********************#



#*******************************************************************************
  # @brief  Panic packet process.
  # @param  reader received data
  # @retval None
  #****************************************************************************#

def Reader_panic_packet_decode(Reader_received_data):
  student_packet = bytearray(16)
  temp=bytearray(2)

  temp[0]=Reader_received_data[74]
  temp[1]=Reader_received_data[75]

  for i in range(int(temp.decode('utf-8'))):
    for j in range(16):
      student_packet[j]=Reader_received_data[Student_beacon_index[i]+j]

    Beacon_packet_string=student_packet.hex()
    Student_beacon_packet_HEX[0]=Beacon_packet_string[0:14]     # Student ID
    Student_beacon_packet_HEX[1]=Beacon_packet_string[14:16]    # Beacon status
    Student_beacon_packet_HEX[2]=Beacon_packet_string[16:22]    # Time
    Student_beacon_packet_HEX[3]=Beacon_packet_string[22:28]    # Date
    Student_beacon_packet_HEX[4]=Beacon_packet_string[28:30]    # Battery voltage in percentage
    Student_beacon_packet_HEX[5]=Beacon_packet_string[30:32]    # RSSI
    temp1=Reader_received_data[9:18]

    m_data_payload="\"SID\":\""+Student_beacon_packet_HEX[0]+"\",\"RID\":\""+temp1.decode('utf-8')+"\""
    #cloud_data_send('1','02',m_data_payload)
    cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_PANIC,m_data_payload]
    Cloud_send_circular_buffer.append(cloud_tx_data)

#******************** End of Reader_panic_packet_decode ********************#


#*******************************************************************************
  # @brief  Reader nfc packet processing.
  # @param  reader received data
  # @retval None
  #****************************************************************************#
def Reader_heartbeat_packet_decode(reader_rx_packet_json):
  for i in range(Reader_count[0]):
    if(Reader_management_data[0][i+1][0] == reader_rx_packet_json["SGID"]):
      Reader_management_data[0][i+1][10]=reader_rx_packet_json["BATR"]
      Reader_management_data[0][i+1][9]=reader_rx_packet_json["STAT"]
 
  #print("Reader_management:",Reader_management_data)

#******************** End of Reader_heartbeat_packet_decode ********************#


#*******************************************************************************
  # @brief  Reader nfc packet processing.
  # @param  reader received data
  # @retval None
  #****************************************************************************#

def Reader_nfc_packet_decode(reader_rx_packet_json):
  for j in range (Student_count[0]):
    if Student_Tracking_table_template[j][0]==reader_rx_packet_json["RFID"]:

      if Student_Tracking_table_template[j][1] == '-':
        Student_Tracking_table_template[j][1]=reader_rx_packet_json["SGID"]
        Student_Tracking_table_template[j][2]=reader_rx_packet_json["TIME"]
      else:
        Student_Tracking_table_template[j][3]=reader_rx_packet_json["SGID"]
        Student_Tracking_table_template[j][4]=reader_rx_packet_json["TIME"]

#******************** End of Reader_nfc_packet_decode ********************#


#*******************************************************************************
  # @brief  Reader hardware alert.
  # @param  reader received data
  # @retval None
  #****************************************************************************#

def Reader_hardware_alert_packet_decode(reader_rx_packet_json):
  for i in range(Reader_count[0]):
    if(Reader_management_data[0][i+1][0] == reader_rx_packet_json["SGID"]):
      Reader_management_data[0][i+1][9]=reader_rx_packet_json["STAT"]
      m_data_payload="\"RID\":\""+reader_rx_packet_json["SGID"]+"\",\"ALT\":\""+reader_rx_packet_json["HWER"]+"\",\"STA\":\""+"1"+"\""
      #cloud_data_send('1','03',m_data_payload)
      cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_READER_ALERT,m_data_payload]
      Cloud_send_circular_buffer.append(cloud_tx_data)


#******************** End of Reader_hardware_alert_packet_decode ********************#


#*******************************************************************************
  # @brief  Reader data receive process.
  # @param  short id, received data
  # @retval None
  #****************************************************************************#

def Reader_receive_process(Reader_short_id,Reader_received_data):
 print("\nReader > Received packet:",Reader_received_data)
 stack_error_indication=0
 if len(Reader_received_data)>=30:
  if chr(Reader_received_data[0]) == '{':

    if chr(Reader_received_data[62]) == '1' and chr(Reader_received_data[63]) == '6':
      Reader_tracking_packet_decode(Reader_short_id,Reader_received_data)

    elif chr(Reader_received_data[62]) == '1' and chr(Reader_received_data[63]) == '7':
      Reader_panic_packet_decode(Reader_received_data)

    else:
      try:
        reader_rx_packet_raw[0]=Reader_received_data.decode('utf-8')   # decoded from byte array to string
      except:
        stack_error_indication=1
        print("Reader Module> Packet Decode Error")

      #print("Data received:",reader_rx_packet_raw[0])
      if reader_rx_packet_raw[0][0] == '{' and '}' in reader_rx_packet_raw[0] and stack_error_indication == 0:
        try:
          reader_rx_packet_json = json.loads(reader_rx_packet_raw[0],encoding='utf-8')
        except ValueError as e:
          stack_error_indication=1
          print("Reader Module> Json value error")
        except IndexError as e:
          stack_error_indication=1
          print("Reader Module> Json Index error")
        except KeyError as e:
          stack_error_indication=1
          print("Reader Module> Json key error")

        if stack_error_indication==0:
          if reader_rx_packet_json["TYPE"] == READER_MESSAGE_TYPE_HEARTBEAT:      
            Reader_heartbeat_packet_decode(reader_rx_packet_json)             

          elif reader_rx_packet_json["TYPE"] == READER_MESSAGE_TYPE_NFC:
            Reader_nfc_packet_decode(reader_rx_packet_json)

          elif reader_rx_packet_json["TYPE"] == READER_MESSAGE_TYPE_HARDWARE_ALERT:
            Reader_hardware_alert_packet_decode(reader_rx_packet_json)

          elif reader_rx_packet_json["TYPE"] == READER_MESSAGE_TYPE_CONFIGURE_RESPONSE:
            print("Reader Module --> Configure response packet")

          else:
            stack_error_indication=0
            print("Reader Module -->ModuleInvalid packet type received")

          if reader_rx_packet_json["TYPE"] in reader_valid_packet_types:
            return 1

      else:
        stack_error_indication=0
        print("Reader Module --> Invalid packet start byte and stop not validated (byte decode)")
  else:
    print("Reader Module --> Invalid packet start byte not validated")
 return 0 
 
#******************** End of Reader_receive_process ********************#


#*******************************************************************************
  # @brief  Reader communication thread.
  # @param  Thread name
  # @retval None
  #****************************************************************************#
def Reader_communication_thread(thread_name):
    while True:
      for i in range (Reader_count[0]):
        try:
          Reader_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error:
          print('\nReader Module> Failed to create socket')
          sys.exit()
        
        try:
          Reader_socket.connect((Reader_management_data[0][i+1][1] , int(Reader_management_data[0][i+1][2]))) # IP and port from reader management file
          print("\nReader Module> Created the connection with:",Reader_management_data[0][i+1][1])
        except:
          print("\nReader Module> Not created the connection with:",Reader_management_data[0][i+1][1])
          continue

        SG_id[0]   = Reader_management_data[0][i+1][0]
        SG_type[0] = READER_MESSAGE_TYPE_HEARTBEAT_RESPONSE
        seqn[0]    = str('{0:02d}'.format(Reader_sequence_number[0]))
        Reader_sequence_number[0]=Reader_sequence_number[0]+1
        if Reader_sequence_number[0] >= 10000:
          Reader_sequence_number[0]=0

        Reader_transmit_packet_form()
        temp_data=str(reader_tx_packet_raw[0])
        
        time.sleep(0.5)  # Delay before First data send
        try:
          Reader_socket.sendall(temp_data.encode('utf-8'))
        except socket.error as e:
          #print ("send error: %s", e)
          print ("Reader Module> Transmit error:", Reader_management_data[0][i+1][1])

        
        while True:        
          try:
            Reader_socket.settimeout(8)
            data = Reader_socket.recv(596)
            if not data:
              print("\nReader Module> Connection close",Reader_management_data[0][i+1][1])
              Reader_socket.close()
              break
            Reader_receive_circular_buffer.append(data)
          except:
            break

        Reader_socket.close()
        time.sleep(1)

#******************** End of Reader communication thread ********************#

#*******************************************************************************
  # @brief  Reader received data processing thread.
  # @param  Thread name
  # @retval None
  #****************************************************************************#

def Reader_data_processing_thread(thread_name):
  while True:
      try:
       temp_str=Reader_receive_circular_buffer.pop()
       Reader_receive_process(0,temp_str)
      except:
        r=1
        #print("\nReader Module> Receive buffer empty")
      
#******************** End of Reader data processing thread ********************#