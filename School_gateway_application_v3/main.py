import _thread
import time
import sys

#### ****************************************** File inclusion ************************************* ### 
from slave_reader import *
from HTTP_Server import *
from cloud_communication import *

### ****************************************** Variable declaration ************************************* ###


### ******************************************** Function prototype ************************************* ###
print ("Calixto > Application starting....")
application_init()

try:
   _thread.start_new_thread(Csv_file_generation_thread,("CSV File generation",))
   _thread.start_new_thread(Gateway_alive_timer_thread,("cloud_communication",))
   _thread.start_new_thread(Gateway_cloud_communication_thread,("cloud_communication",))
   _thread.start_new_thread(Reader_communication_thread,("Reader_communication",))
   _thread.start_new_thread(Reader_data_processing_thread,("Reader_data_processing",))
   #thread.start_new_thread( Web_server_thread,("webserver",))
   if TARGET_DEVICE == TARGET_DEVICE_CALIXTO_GATEWAY:
      _thread.start_new_thread(LEDs_pattern_indicator_thread,("LED status",))
   
except:
   print ("Error: unable to start thread")

while 1:
   pass
   

