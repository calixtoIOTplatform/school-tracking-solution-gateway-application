from flask import Flask, render_template
import _thread
import time

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('first_page.html')

@app.route('/forward/',methods=['POST'])
def hello_world1():
    return 'Welcome Home'

#3--> Web server communication thread 
def Web_server_thread(threadname):
    app.run(host = 'localhost',port = 1000)
    while 1:
      i=0