import sys
import time
import pickle
import datetime
import csv
import shutil
import os
import requests
import json
import paramiko

#### ****************************************** File inclusion ************************************* ### 
from common_include import *
          

### ****************************************** Variable declaration ************************************* ###
cloud_data_payload=["\"PLD\""]
cloud_tx_packet_fields=["1","02","FE12344565567678","112922","17012019"]
cloud_tx_packet_raw = ["{\"MCL\":\""+cloud_tx_packet_fields[0]+"\",\"MTP\":\""+cloud_tx_packet_fields[1]+"\",\"GID\":\""+cloud_tx_packet_fields[2]+"\","+cloud_data_payload[0]+",\"DAT\":\""+cloud_tx_packet_fields[3]+"\",\"TIM\":\""+cloud_tx_packet_fields[4]+"\"}"]

#url = 'http://staging.bhumithilabs.com/api/event/'
url = 'http://122.165.155.203:80/api/event/'        #Static IP of XORBIUM
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
localtime = time.asctime( time.localtime(time.time()) )
data = {"data_send_count": str(1),"Current_Date_Time":localtime}


### ******************************************** Function prototype ************************************* ###


#*******************************************************************************
  # @brief  cloud reception process.
  # @param  cloud recieved data packet
  # @retval None
  #****************************************************************************#

def cloud_receive_process(cloud_rx_packet):
  print("\nCloud Module > Received packet ",cloud_rx_packet)
  cloud_rx_packet_json = json.loads(cloud_rx_packet,encoding='utf-8')
  Trigger_response_packet=[0]
  
  if cloud_rx_packet_json["GID"]== Gateway_management_data[0][1][0]:        # Check Gateway 

    if cloud_rx_packet_json["MCL"] == CLOUD_MESSAGE_CLASS_FUNCTIONING:      # Normal Functioning class
    
      if cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_ACK:
        print("Cloud Module> ACK Packet received")
      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_NACK:
        print("Cloud Module> NACK Packet received")
      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_CLOUD_ALERT:
        print("Cloud Module> CLOUD ALERT Packet received")

    elif cloud_rx_packet_json["MCL"] == CLOUD_MESSAGE_CLASS_CONFIGURATION:   # Configuration class

      if cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_CONFIGURE:
        Store_Gateway_configuration(cloud_rx_packet_json)
        m_data_payload="\"CMD\":\""+CLOUD_MESSAGE_TYPE_CONFIGURE+"\",\"STA\":\""+"1"+"\",\"MSG\":\""+"xxxx"+"\""

      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_DOWNLOAD_STUDENT_IDS:
        Store_Student_ids(cloud_rx_packet_json)
        m_data_payload="\"CMD\":\""+CLOUD_MESSAGE_TYPE_DOWNLOAD_STUDENT_IDS+"\",\"STA\":\""+"1"+"\",\"MSG\":\""+"xxxx"+"\""
        

      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_UPDATE_STUDENT_IDS:
        m_data_payload="\"CMD\":\""+CLOUD_MESSAGE_TYPE_UPDATE_STUDENT_IDS+"\",\"STA\":\""+"1"+"\",\"MSG\":\""+"xxxx"+"\""
        print("Cloud Module> Update student IDs Packet received")

      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_DOWNLOAD_ALERT_RULES:
        Store_alert_rules(cloud_rx_packet_json)
        m_data_payload="\"CMD\":\""+CLOUD_MESSAGE_TYPE_DOWNLOAD_ALERT_RULES+"\",\"STA\":\""+"1"+"\",\"MSG\":\""+"xxxx"+"\""
        

      elif cloud_rx_packet_json["MTP"] == CLOUD_MESSAGE_TYPE_FIRMWARE_UPDATE:
        m_data_payload="\"CMD\":\""+CLOUD_MESSAGE_TYPE_FIRMWARE_UPDATE+"\",\"STA\":\""+"1"+"\",\"MSG\":\""+"xxxx"+"\""
        print("Cloud Module> firmware update Packet received")

      else:
        Trigger_response_packet[0]=1
  
      #cloud_data_send(CLOUD_MESSAGE_CLASS_CONFIGURATION,CLOUD_MESSAGE_TYPE_STATUS_UPDATE,m_data_payload)
      if Trigger_response_packet[0]==0:
        cloud_tx_data=[CLOUD_MESSAGE_CLASS_CONFIGURATION,CLOUD_MESSAGE_TYPE_STATUS_UPDATE,m_data_payload]
        Cloud_send_circular_buffer.append(cloud_tx_data)
  else:
    print("Cloud Module> Gateway ID is not matched")
#************************** End of cloud reception process *******************************#



#*******************************************************************************
  # @brief  send data to cloud.
  # @param  Class, Type, message payload
  # @retval None
  #****************************************************************************#

def cloud_data_send(m_class,m_type,m_data_payload):
  cloud_tx_packet_fields[0]=m_class
  cloud_tx_packet_fields[1]=m_type
  now = datetime.datetime.now()
  cloud_tx_packet_fields[3]=str('{0:02d}'.format(now.day))+str('{0:02d}'.format(now.month))+str('{0:04d}'.format(now.year))
  cloud_tx_packet_fields[4]=str('{0:02d}'.format(now.hour))+str('{0:02d}'.format(now.minute))+str('{0:02d}'.format(now.second))
  cloud_data_payload[0]=m_data_payload
  cloud_tx_packet_raw[0] = "{\"MCL\":\""+cloud_tx_packet_fields[0]+"\",\"MTP\":\""+cloud_tx_packet_fields[1]+"\",\"GID\":\""+Gateway_management_data[0][1][0]+"\","+m_data_payload+",\"DAT\":\""+cloud_tx_packet_fields[3]+"\",\"TIM\":\""+cloud_tx_packet_fields[4]+"\"}"
  #print("\nCloud_send > ",json.loads(cloud_tx_packet_raw[0]))
  print("\nCloud_send > ",cloud_tx_packet_raw[0])
  try:
    Gateway_error_indication[1]=0
     #data={"MCL":"1","MTP":"01","GID":"EE12341565567618","FLN":"student_info_20190207_140245.csv","SIZ":"00000001","DAT":"07022019","TIM":"140246"}
    r = requests.post(url, data=cloud_tx_packet_raw[0], headers=headers,timeout=5)   
  except:
     print("Cloud> send Error")
     Gateway_error_indication[1]=1 
  
  if Gateway_error_indication[1]==0:
    cloud_receive_process(r.text)
  

#************************** End of send data to cloud *******************************#
  


#*******************************************************************************
  # @brief  CSV file generation thread.
  # @param  thread name
  # @retval None
  #****************************************************************************#

def Csv_file_generation_thread(threadname):
  while True:
   #student_entry_reported[0] =1
   if student_entry_reported[0] ==1:
    localtime = time.asctime( time.localtime(time.time()) )
    now = datetime.datetime.now()
    student_track_filename='student_info_'+str('{0:04d}'.format(now.year))+str('{0:02d}'.format(now.month))+str('{0:02d}'.format(now.day))+'_'+str('{0:02d}'.format(now.hour))+str('{0:02d}'.format(now.minute))+str('{0:02d}'.format(now.second))+'.csv'

    with open(student_track_filename, 'w', newline='') as csvfile:
      filewriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
    
      filewriter.writerow(['','',GATEWAY_ID[0],localtime,'Calixto systems pvt ltd'])
      filewriter.writerow(['STUDENT ID','FIRST READER ID','FIRST SEEN','LAST READER ID','LAST SEEN','ATTENDANCE READER ID','ATTENDANCE','BATTERY'])
      for i in range(Student_count[0]):
        filewriter.writerow(Student_Tracking_table_template[i])

      #print("data test",Student_Tracking_table_template)
      time.sleep(1)  #file cration time delay
      csvfile.close()
      Clear_student_track_table()
      csv_file_size = os.path.getsize(student_track_filename)/1024
      if int(csv_file_size) <= 0:
        csv_file_size=1
      #print("data test",Student_Tracking_table_template)
      ###### ************ move file to Tracking_Files directory ************** ########
      shutil.move(student_track_filename, 'Data_storage/Student_tracking_files/')

      ###### ************ File upload to SFTP Server ************************ #######
      transport = paramiko.Transport(sftp_server_details[0], sftp_server_details[1])
      try:              
       transport.connect(username = sftp_server_details[2], password = sftp_server_details[3])
       sftp = paramiko.SFTPClient.from_transport(transport)
       sftp.chdir('uploads/calixto/Student_tracking_files')
       files = sftp.listdir()
       print (files)
       sftp.put(localpath='Data_storage/Student_tracking_files/'+student_track_filename, remotepath=student_track_filename)
       files = sftp.listdir()
       print (files)
       sftp.close()
       transport.close()
       print('cloud> File Upload Done')
      except:
       print("cloud> File Upload error")
      
      ###### ************ Get file size and make application payload ************** ########      
      m_data_payload="\"FLN\":\""+student_track_filename+"\",\"SIZ\":\""+str('{0:08d}'.format(csv_file_size))+"\""
      cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_TRACKING_FILE_UPLOAD,m_data_payload]
      Cloud_send_circular_buffer.append(cloud_tx_data)
      #cloud_data_send('1','01',m_data_payload)
      student_entry_reported[0]=0
   time.sleep(csv_file_generation_interval[0])

#************************** End of send data to cloud *******************************#


#*******************************************************************************
  # @brief  Gateway alive message.
  # @param  thread name
  # @retval None
  #****************************************************************************#
def Gateway_alive_timer_thread(threadname):
  while True:
    m_data_payload="\"HTH\":\"00\""
    cloud_tx_data=[CLOUD_MESSAGE_CLASS_FUNCTIONING,CLOUD_MESSAGE_TYPE_GATEWAY_ALIVE,m_data_payload]
    Cloud_send_circular_buffer.append(cloud_tx_data)
    time.sleep(Gateway_alive_interval[0])

#************************** End of gateway alive timer thread *******************************#

#*******************************************************************************
  # @brief  Cloud communication thread.
  # @param  thread name
  # @retval None
  #****************************************************************************#
def Gateway_cloud_communication_thread(threadname):
  while True:
    try:
       temp_str=Cloud_send_circular_buffer.pop()
       cloud_data_send(temp_str[0],temp_str[1],temp_str[2])
    except:
        r=1
    #time.sleep(1)

#************************** End of Cloud communication thread *******************************#